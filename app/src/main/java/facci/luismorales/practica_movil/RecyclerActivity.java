package facci.luismorales.practica_movil;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;

import java.util.List;

public class RecyclerActivity extends AppCompatActivity {
    RecyclerView recyclerViewProfesores;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        recyclerViewProfesores = findViewById(
                R.id.RecyclerViewProfesores);
//        Coleccion de Datos
        List<Profesor> listaProfesores = Profesor.listAll(
                Profesor.class);
        listaProfesores.forEach(
                (item)-> Log.e("Profesor", item.getNombre()));
//        Layout
        recyclerViewProfesores.setLayoutManager(
                new LinearLayoutManager(this)
        );
//        Adatador
        recyclerViewProfesores.setAdapter(new RecyclerView.Adapter() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return null;
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

            }

            @Override
            public int getItemCount() {
                return 0;
            }
        });
    }
}