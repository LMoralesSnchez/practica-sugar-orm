package facci.luismorales.practica_movil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText editTextApellidos;
    EditText editTextNombres;
    EditText editTextFoto;
    Button buttonGuardar;
    Button buttonRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextApellidos = findViewById(R.id.EditTextApellidos);
        editTextNombres = findViewById(R.id.EditTextNombres);
        editTextFoto = findViewById(R.id.EditTextFoto);

        buttonGuardar = findViewById(R.id.ButtonGuardar);
        buttonRecycler = findViewById(R.id.ButtonRecycler);

        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Profesor profesor = new Profesor(
                        editTextApellidos.getText().toString(),
                        editTextNombres.getText().toString(),
                        editTextFoto.getText().toString()
                );
                profesor.save();

                Log.e("SQLite","DATOS GUARDADOS");
            }
        });

        buttonRecycler.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(MainActivity.this,
                        RecyclerActivity.class);
                startActivity(intent);
            }
        });
    }

}